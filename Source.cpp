//https://www.geeksforgeeks.org/external-sorting/
//https://en.wikipedia.org/wiki/External_sorting
// C++ program to implement external sorting using 
// merge sort 
#define _CRT_SECURE_NO_WARNINGS
#ifndef _MSC_VER
#include <sys/statvfs.h>
#else
#include <Windows.h>
#endif
#include <iostream>
#include <algorithm>
#include <cstdio>
#include <cstdlib>
#include <fstream>
#include <string>
#include <iomanip>
#include <cmath>
#include <array>
#include <future>
#include <utility>

struct MinHeapNode
{
	// The element to be stored 
	uint32_t element;

	// index of the array from which the element is taken 
	int32_t i;
};


// A class for Min Heap 
class MinHeap
{
	//MinHeapNode* harr; // pointer to array of elements in heap 
	std::vector<MinHeapNode> harr;
	int32_t heap_size;	 // size of min heap 

public:
	// Constructor: creates a min heap of given size 
	MinHeap(std::vector<MinHeapNode> a);

	// to heapify a subtree with root at given index 
	void MinHeapify(int32_t);

	// to get index of left child of node at index i 
	int32_t left(int32_t i) { return (2 * i + 1); }

	// to get index of right child of node at index i 
	int32_t right(int32_t i) { return (2 * i + 2); }

	// to get the root 
	MinHeapNode getMin() { return harr[0]; }

	// to replace root with new node x and heapify() 
	// new root 
	void replaceMin(MinHeapNode x)
	{
		harr[0] = x;
		MinHeapify(0);
	}
};

// Constructor: Builds a heap from a given array a[] 
// of given size 
MinHeap::MinHeap(std::vector<MinHeapNode> a) : harr(std::move(a)), heap_size(harr.size()-1)
{
	int32_t i = (heap_size - 1) / 2;
	while (i >= 0)
	{
		MinHeapify(i);
		i--;
	}
}

// A recursive method to heapify a subtree with root 
// at given index. This method assumes that the 
// subtrees are already heapified 
void MinHeap::MinHeapify(const int32_t i)
{
	int32_t l = left(i);
	int32_t r = right(i);
	int32_t smallest = i;
	if (l < heap_size && harr[l].element < harr[i].element)
		smallest = l;
	if (r < heap_size && harr[r].element < harr[smallest].element)
		smallest = r;
	if (smallest != i)
	{
		std::iter_swap(harr.begin() + i, harr.begin() + smallest);
		MinHeapify(smallest);
	}
}


class ExternalSortClass
{
public:
	ExternalSortClass() = delete;
	ExternalSortClass(std::string inputFile, std::string outputFile);
	~ExternalSortClass() = default;

private:

	std::string InputFileName;
	std::string OutputFileName;
	uint32_t TotalFilesToCreate = 0;
	uint32_t MaxThread = 0;
	uint32_t RunSize = 0;
	uint32_t ArraySize = 0;
	void MergeFiles(uint32_t totalFiles) const;
	void GetArraySize();
	void GetRunSize();
	//In Mb
	uint32_t GetMemFree();
	// For sorting data stored on disk 
	void ExternalSort();
	size_t GetFileSize() const;

	static void CreateSortChunkFile(std::vector<uint32_t> arr, size_t end_position, const uint32_t next_output_file);

	// Using a merge-sort algorithm, create the initial runs 
// and divide them evenly among the output files 
	uint32_t CreateInitialRuns() const;
};


ExternalSortClass::ExternalSortClass(std::string inputFile, std::string outputFile) : InputFileName(
	std::move(inputFile)), OutputFileName(std::move(outputFile))
{
	GetRunSize();
	GetArraySize();
	ExternalSort();
}

void ExternalSortClass::MergeFiles(const uint32_t totalFiles) const
{
	constexpr uint32_t uint_max = 0xffffffff;
	std::vector< std::ifstream> in(totalFiles);
	for (uint32_t i = 0; i < totalFiles; ++i)
	{
		in[i] = std::ifstream(std::to_string(i), std::ios::binary | std::istream::in);
	}
	// FINAL OUTPUT FILE 
	auto out = std::ofstream(OutputFileName, std::ios::binary | std::istream::out);

	// Create a min heap with k heap nodes. Every heap node 
	// has first element of scratch output file
	std::vector< MinHeapNode> harr(totalFiles);
	uint32_t i, tmp;

	for (i = 0; i < totalFiles; ++i)
	{
		// break if no output file is empty and 
		// index i will be no. of input files 

		if (in[i].read(reinterpret_cast<char*>(&tmp), sizeof(uint32_t)))
			harr[i].element = tmp;
		else
			break;

		harr[i].i = i; // Index of scratch output file 
	}
	MinHeap hp(std::move(harr)); // Create the heap 

	uint32_t count = 0;

	// Now one by one get the minimum element from min 
	// heap and replace it with next element. 
	// run till all filled input files reach EOF 
	constexpr int64_t bufSize = 256;
	std::array< uint32_t, bufSize> buf{};
	int64_t counter = 0;
	while (count != i)
	{
		// Get the minimum element and store it in output file 
		auto root = hp.getMin();
		buf[counter] = root.element;
		if (counter == bufSize - 1)
		{
			out.write(reinterpret_cast<const char*>(buf.data()), sizeof(uint32_t) * bufSize);
			counter = -1;
		}

		// Find the next element that will replace current 
		// root of heap. The next element belongs to same 
		// input file as the current min element. 
		if (in[root.i].read(reinterpret_cast<char*>(&tmp), sizeof(uint32_t)))
			root.element = tmp;
		else
		{
			root.element = uint_max;
			++count;
		}

		// Replace root with next element of input file 
		hp.replaceMin(root);
		++counter;
	}
	if (counter > 0)
		out.write(reinterpret_cast<const char*>(buf.data()), sizeof(uint32_t) * counter);
	// close input and output files 
	for (uint32_t i1 = 0; i1 < totalFiles; ++i1)
	{
		in[i1].close();
	}
	out.close();
}

void ExternalSortClass::GetArraySize()
{
	ArraySize = static_cast<uint32_t>(ceil(RunSize / sizeof(uint32_t)));
	TotalFilesToCreate = (GetFileSize() / sizeof(uint32_t)) / ArraySize;
}

void ExternalSortClass::GetRunSize()
{
	auto freeRam = GetMemFree();
#ifdef MY
	freeRam = 80;
#endif // MY
	const auto ramAvailableForProgram = static_cast<uint32_t>(freeRam - freeRam * 0.2);
	MaxThread = std::thread::hardware_concurrency() - 2;
	if (MaxThread > 8)
		MaxThread = 8;
	if (MaxThread <= 0)
		MaxThread = 1;
	RunSize = (ramAvailableForProgram / MaxThread) * 1024 * 1024;
}

uint32_t ExternalSortClass::GetMemFree()
{
	try
	{
#ifndef _MSC_VER
		std::string token;
		std::ifstream file("/proc/meminfo");
		if (file.is_open())
			while (file >> token)
			{
				if (token == "MemFree:")
				{
					unsigned long mem;
					if (file >> mem)
					{
						return static_cast<uint32_t>(mem / 1024);
					}
				}
				// ignore rest of the line
				file.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
			}
#else
		MEMORYSTATUSEX statex;
		statex.dwLength = sizeof(statex);
		if (GlobalMemoryStatusEx(&statex) != 0)
			return static_cast<uint32_t>(statex.ullAvailPhys / (1024 * 1024));
#endif
	}
	catch (const std::exception&)
	{
		return 100; // nothing found
	}

	return 100; // nothing found
}

void ExternalSortClass::ExternalSort()
{
	const auto totalFiles = CreateInitialRuns();
	MergeFiles(totalFiles);
}

size_t ExternalSortClass::GetFileSize() const
{
	std::ifstream in(InputFileName, std::ifstream::ate | std::ifstream::binary);
	const auto result = in.tellg();
	if (result > 0)
	{
		if (in.is_open())
			in.close();
		return result;
	}

	auto out = std::fstream(OutputFileName, std::ios::binary | std::istream::out);
	out.close();
	exit(EXIT_FAILURE);
}

void ExternalSortClass::CreateSortChunkFile(std::vector<uint32_t> arr, size_t end_position, const uint32_t next_output_file)
{
	auto out = std::ofstream(std::to_string(next_output_file), std::ios::binary | std::istream::out);
	std::sort(arr.begin(), arr.begin() + end_position);
	out.write(reinterpret_cast<const char*>(arr.data()), sizeof(uint32_t) * end_position);
	out.close();
}

uint32_t ExternalSortClass::CreateInitialRuns() const
{
	uint32_t end_position;
	uint32_t baseSizeFromFile;
	int64_t run_sizeTmp = RunSize;
	uint32_t totalFiles = 0;
	uint32_t currentFiles = 0;
	size_t myCounter = 0;
	bool more_input = true;
	// For big input file 
	auto in = std::ifstream();
	in.open(InputFileName, std::ios::binary | std::istream::in);
	std::vector< std::future<void>> myThreads(MaxThread);
	constexpr std::chrono::milliseconds ms5(5);
	std::vector<uint32_t> baseArray(ArraySize);
	
	while (more_input)
	{
		while (true)
		{
			if (myThreads[myCounter].valid())
			{
				const auto status = myThreads[myCounter].wait_for(ms5);
				if (status != std::future_status::ready)// 
					++myCounter;
				else
					break;
			}
			else
				break;
			if (myCounter > MaxThread - 1)
				myCounter = 0;
		}
		end_position = 0;
		if (baseArray.size() == 0)
			baseArray.resize(ArraySize);
		// write run_size elements into arr from input file 
		if (TotalFilesToCreate > currentFiles)
		{

			in.read(reinterpret_cast<char*>(baseArray.data()), sizeof(uint32_t) * ArraySize);
			++currentFiles;
			end_position = ArraySize;
		}
		else//
		{
			while (0 < run_sizeTmp)
			{
				if (in.read(reinterpret_cast<char*>(&baseSizeFromFile), sizeof(uint32_t)))
				{
					run_sizeTmp = run_sizeTmp - sizeof(uint32_t);
					baseArray[end_position] = baseSizeFromFile;
				}
				else
				{
					more_input = false;
					break;
				}
				++end_position;
			}
		}
		// sort array using merge sort 
		myThreads[myCounter] = std::async(std::launch::async, &ExternalSortClass::CreateSortChunkFile, std::move(baseArray), end_position, totalFiles);
		++totalFiles;
	}
	for (uint16_t currentThread = 0; currentThread < MaxThread; ++currentThread)
	{
		if (myThreads[currentThread].valid())
			myThreads[currentThread].wait();
	}
	in.close();
	return totalFiles;
}


int main(int argc, char* argv[])
{
#ifdef MY
	const clock_t t1 = clock();
	const std::string input_file = R"(F:\Sample\input)";
	const std::string output_file = R"(F:\Sample\output)";
#else
	const std::string input_file = "input";
	const std::string output_file = "output";
#endif
	ExternalSortClass sort(input_file, output_file);
#ifdef MY
	const clock_t t2 = clock();
	std::cout << (t2 - t1 + .0) / CLOCKS_PER_SEC << std::endl;
	system("pause");
#endif
	return EXIT_SUCCESS;
}
